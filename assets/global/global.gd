extends Node

var build_length = 1

signal length_changed

func change_build_length(val):
	if val == -1 and build_length > 1:
		build_length += val
	if val == +1 and build_length < 20:
		build_length += val
	emit_signal("length_changed")