extends Spatial

var invisible = 0.2

var arrows = []

func _ready():
	arrows.append($x_plus)
	arrows.append($x_minus)
	arrows.append($z_plus)
	arrows.append($z_minus)			
	
	for i in arrows:
		i.get_surface_material(0).set_shader_param("opacity", invisible)

func _on_StaticBody_input_event(camera, event, click_position, click_normal, shape_idx):
	pass # Replace with function body.

## mouse click stuff
func _on_x_input_event(camera, event, click_position, click_normal, shape_idx):
	if Input.is_action_just_pressed("lmb"):
		build(0)

func _on_x_minus_input_event(camera, event, click_position, click_normal, shape_idx):
	if Input.is_action_just_pressed("lmb"):
		build(1)


func _on_z_input_event(camera, event, click_position, click_normal, shape_idx):
	if Input.is_action_just_pressed("lmb"):
		build(2)


func _on_z_minus_input_event(camera, event, click_position, click_normal, shape_idx):
	if Input.is_action_just_pressed("lmb"):
		build(3)


## mouse over stuff
func _on_x_mouse_entered():
	play_click()
	arrows[0].get_surface_material(0).set_shader_param("opacity", 1)

func _on_x_mouse_exited():
	arrows[0].get_surface_material(0).set_shader_param("opacity", invisible)

func _on_x_minus_mouse_entered():
	play_click()
	arrows[1].get_surface_material(0).set_shader_param("opacity", 1)


func _on_x_minus_mouse_exited():
	arrows[1].get_surface_material(0).set_shader_param("opacity", invisible)


func _on_z_mouse_entered():
	play_click()	
	arrows[2].get_surface_material(0).set_shader_param("opacity", 1)


func _on_z_mouse_exited():
	arrows[2].get_surface_material(0).set_shader_param("opacity", invisible)


func _on_z_minus_mouse_entered():
	play_click()
	arrows[3].get_surface_material(0).set_shader_param("opacity", 1)


func _on_z_minus_mouse_exited():
	arrows[3].get_surface_material(0).set_shader_param("opacity", invisible)
	
func build(val):
	var build_length = global.build_length
	if build_length == 1:
		get_parent().build(val)
	if build_length > 1:
		get_parent().build_multiple(val)
		
func play_click():
	get_parent().mouse_over()
