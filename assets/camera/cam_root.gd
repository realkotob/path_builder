extends Spatial

export(float) var smoothness = 10
export(int) var sensitivity = 0.02

var mouse_speed = Vector2()
var mouse_smooth = Vector2()

var camera_angle = 0
var angle_change = 0

var target_pos = Vector3()
var pos = Vector3()

var move_speed = 1

func _ready():
	target_pos = self.global_transform.origin
	pos = self.global_transform.origin
	
func _process(delta):
	if Input.is_mouse_button_pressed(2):
		mouse_look(delta)
	smooth_move(delta)

func mouse_look(delta):
	mouse_smooth = mouse_smooth.linear_interpolate(mouse_speed, smoothness * delta)

	self.rotate_y(rad2deg(-mouse_smooth.x * sensitivity * delta))

	angle_change = mouse_smooth.y * sensitivity * delta
	
	if camera_angle + angle_change > -0.027 and camera_angle + angle_change < 0.027:

		self.rotate_object_local(Vector3(1,0,0), rad2deg(-mouse_smooth.y * sensitivity * delta))
		camera_angle += angle_change


	mouse_speed = Vector2()

func _input(event):
	if event is InputEventMouseMotion:
		mouse_speed = event.relative
		
func smooth_move(delta):
	pos = pos.linear_interpolate(target_pos, move_speed * delta)
	self.global_transform.origin = pos

func _on_cubee_builder_block_built(pos):
	target_pos = pos

func _on_cubee_builder_block_removed(pos):
	target_pos = pos
